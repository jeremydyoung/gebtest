package com.ean.backoffice

import geb.Page

class CommandCenterPage extends Page {

    static url = '/'
    static at = { title == 'Command Center' }
    static content = {
        // Many more definitions could be added here to represent other links and page structure
        reservations { $('div.ac-white-box', 2) }
        shipinator { reservations.$('a', text: 'Shipinator') }
    }
}
