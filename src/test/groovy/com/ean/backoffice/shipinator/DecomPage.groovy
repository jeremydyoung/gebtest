package com.ean.backoffice.shipinator

import geb.Page

class DecomPage extends Page {

    static url = '/tn/shipinator/search.jsp'
    static at = { title == 'This Application has been Decommissioned' }
    static content = {
        sorry { $('h2') }
    }
}
