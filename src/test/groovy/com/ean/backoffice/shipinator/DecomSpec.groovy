package com.ean.backoffice.shipinator

import geb.spock.GebSpec

import org.openqa.selenium.security.UserAndPassword

import com.ean.backoffice.CommandCenterPage;
import com.sun.java.util.jar.pack.Driver;

class DecomSpec extends GebSpec {

    def "Load Command Center"() {
        when:
        to CommandCenterPage

        then:
        reservations.find('a').size() == 13
        shipinator.text() == 'Shipinator'
    }

    def "Click Shipinator Link"() {
        when:
        to DecomPage

        then:
        sorry.text() == 'We are sorry, the application you are trying to access is no longer available.'
    }

    def "Load Tracking Script"() {
        given:
        to DecomPage

        when:
        go $('head script').@src

        then:
        $('pre').text() == "// test    SHIPINATOR"
    }
}
