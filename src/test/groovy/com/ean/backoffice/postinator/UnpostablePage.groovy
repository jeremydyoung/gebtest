package com.ean.backoffice.postinator

import geb.Page

class UnpostablePage extends Page {

    static url = '/tn/postinator/unpostable.jsp'
    static at = {
        title =~ /Unpostable Collections -- /
    }
    static content = {
        records(required: false) { moduleList UnpostableRecord, $('a') }
    }
}
