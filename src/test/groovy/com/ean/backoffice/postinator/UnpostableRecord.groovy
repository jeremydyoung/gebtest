package com.ean.backoffice.postinator

import geb.Module
import groovy.json.StringEscapeUtils;

class UnpostableRecord extends Module {
    static content = {
        link { $('img').parent() }
        jsFunc { link.@href }
        jsList { StringEscapeUtils.unescapeJavaScript(URLDecoder.decode((jsFunc =~ /(javascript:UpdateRec\()(.*)(\).*)/)[0][2], "UTF-8")).replaceAll(/\s+/, " ") }
        jsItems { jsList.split("',").collect { it.replaceAll("'", "") } }
        jsExceptID { jsItems[0] }
        jsEmployee { jsItems[1] }
        jsAmount { jsItems[2] }
        jsType { jsItems[3] }
        jsDesc { jsItems[4] }
        jsHotelID { jsItems[5] }
        fonts { link.nextAll('font') }
        amount { fonts[0] }
        type { fonts[1] }
        employee { fonts[2] }
        desc { fonts[3] }
        hotelID { fonts[4] }
    }
}
