package com.ean.backoffice.postinator

import geb.spock.GebSpec

class UnpostableSpec extends GebSpec {

    def "Load Zero Unpostables"() {
        when:
        to UnpostablePage, postingType: 'hotel', depositLogID: '999999', depositID: '9999999'

        then:
        records.size() == 0
    }

    def "Load Unpostables"() {
        when:
        to UnpostablePage, postingType: 'hotel', depositLogID: '27726', depositID: '450494'

        then:
        records.size() == 3
        records[0].tag() == 'a'
        records[0].@href ==~ /javascript:UpdateRec.*/
        records[0].jsFunc ==~ /javascript:UpdateRec.*/
        records[0].jsList == "'51343','2542','0.00','0','Figured out b\\u. dg 2/8/2013','374331'"
        records[0].jsExceptID == '51343'
        records[0].link.next().tag() == 'font'
        records[0].amount.text() == '$' + records[0].jsAmount
        records[0].type.text() =~ /^\(${records[0].jsType}\)/
        records[0].employee.text() == 'Jeremy Young'
        records[0].jsEmployee == '2542'
        records[0].jsDesc == records[0].desc.text() 
        records[0].hotelID.text() == '374331'
        records[1].amount.text() == '$177.40'
        records[2].amount.text() == '$1,234,567,936.00'
    }
}
