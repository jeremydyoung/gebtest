# Geb Test
*by Jeremy D. Young*

## Running the Tests

Prerequisites:

* You'll need the back office war deployed and configured properly.
* Update the baseUrl in the build.gradle if needed.
* Firefox executable will need to be on the path
* Chrome Driver (http://chromedriver.storage.googleapis.com/index.html) will need to be on the path as well
* This project relies upon the gradle wrapper to build, and is included

### Executing Gradle in Linux / Cygwin / Git Bash
```bash
chmod 755 gradlew
./gradlew test
```

### Executing Gradle in Windows cmd
```cmd
gradlew test
```

## Technologies Used

* Gradle 2.2
* Groovy 2.3.6
* Spock 0.7
* Geb 0.10.0
* Selenium Web Driver 2.44.0

## Page Model

I used the basic page modelling included in Geb to represent the urls and basic structure of the pages involved.
There are basic assertions and variables being defined in the Pages that are then available to use in the tests.

## Spock Specifications

There is only one specification file, which contains 3 tests at this time.  These tests assert that the pages involved can be loaded and contain expected data.